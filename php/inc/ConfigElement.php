<?php

final class ArcemuConfigElement implements IteratorAggregate
{
	private $ValuePairs = array();

	public function __construct( $ValuePairs )
	{
		if( !is_array( $ValuePairs ) )
		{
			trigger_error( "Please pass an array when constructing the object.", E_USER_ERROR );
			return;
		}

		$this->ValuePairs = $ValuePairs;
	}

	public function __get( $Name )
	{
		if( !isset( $this->ValuePairs[$Name] ) )
		{
			trigger_error( "There is no element named '$Name'.", E_USER_ERROR );
			return;
		}

		return $this->ValuePairs[$Name];
	}

	public function __set( $Name, $Value )
	{
		$this->ValuePairs[$Name] = $Value;
	}

	public function getIterator()
	{
		$itr = new ArrayObject( $this->ValuePairs );
		return $itr->getIterator();
	}
}

?>