<?php

final class ArcemuConfigFile
{
	private $ConfigData = array();
	private $FilePath;

	/**
	 * Constructs a new ConfigFile object and automatically loads the specified file.
	 *
	 * @param FilePath : A string containing the absolute or relative path to the config file.
	 *
	 * @return nothing
	*/
	public function __construct( $FilePath )
	{
		require_once( "./inc/ConfigElement.php" );
		$this->FilePath = $FilePath;
		$this->Parse();
	}

	public function __destruct()
	{
		unset( $this->ConfigData );
	}

	/**
	 * Returns the value of the specified variable.
	 *
	 * @param Name : The name of the section to use.
	 *
	 * @return null if the section/setting pair do not exist, otherwise gets the an instance of ArcemuConfigElement containing the section's data.
	*/
	public function __get( $Name )
	{
		if( isset( $this->ConfigData[$Name] ) )
			return $this->ConfigData[$Name];
		else
		{
			trigger_error( "No such setting group '$Name' exists.", E_USER_ERROR );
			return null;
		}
	}

	/**
	 * Sets the path of the config file to read/write from.
	 *
	 * @param NewPath : A string containing the absolute or relative path of the new config file.
	 * @param Reparse : Boolean value indicating whether to read from the new file and load it's data.
	 *
	 * @return nothing, or FALSE on failure.
	*/
	public function SetFilePath( $NewPath )
	{
		$this->FilePath = $NewPath;
	}

	/**
	 * Gets the current file's path.
	 *
	 * @return A string containing the path of the current file.
	*/
	public function GetFilePath()
	{
		return $this->FilePath;
	}

	/**
	 * Saves the current data in the array to a file. Overwriting an existing file, or creating a new one if none exists.
	 *
	 * @param Create : Boolean value indicating whether or not to create the file if it does not exist.
	 *
	 * @return TRUE if success, FALSE on failure.
	*/
	public function Save( $Create = true )
	{
		$Handle;

		if( !file_exists($this->FilePath) )
		{
			if( !$Create )
			{
				trigger_error( "The specified file could not be found.", E_USER_ERROR );
				return false;
			}
		}

		if( !( $Handle = fopen( $this->FilePath, "wb" ) ) )
		{
			trigger_error("The file could not be opened/created.", E_USER_ERROR);
			return false;
		}

		$ConfigData = "";

		//The following isn't much to look at from a programming standpoint, but it at least
		//produced easy to read .conf files...

		foreach( $this->ConfigData as $SectionName => $Settings )
		{
			$ConfigData .= "<$SectionName";
			$First = true;

			foreach( $Settings as $Key => $Value )
			{
				if( !$First )
					$ConfigData .= PHP_EOL . "\t$Key = \"$Value\"";
				else
				{
					$ConfigData .= " $Key = \"$Value\"";
					$First = false;
				}
			}

			$ConfigData .= ">" . PHP_EOL . PHP_EOL;
		}

		fwrite( $Handle, $ConfigData );
		fclose( $Handle );
	}

	public function Parse()
	{
		if( !file_exists( $this->FilePath ) )
		{
			trigger_error( "Cannot find file '$this->FilePath'", E_USER_ERROR );
			return;
		}

		if( !( $Handle = fopen( $this->FilePath, "rb" ) ) )
		{
			trigger_error( "Could not open file.", E_USER_ERROR );
			return;
		}

		$this->ConfigData = array(); //Make sure we work with a blank slate each time.
		$Contents = fread( $Handle, filesize( $this->FilePath ) );
		fclose( $Handle );

		//This is where the fun begins...
		$Contents = preg_replace( "#/\*(.*?)\*/#s", "", $Contents ); //Strip out the comments.
		$Contents = preg_replace( "#[\r\n\t]#", "", $Contents ); //No line terminators or tab keys.
		$Contents = preg_replace( "#\s{2,}#", " ", $Contents ); //Two or more whitespace characters are replaced with one.
		$Contents = preg_replace( "#\s+=\s+#", "=", $Contents );

		//Now we really get down and dirty with some regular expressions.
		preg_match_all( "#\<{1}(?P<SectionName>[a-zA-Z]+\w)\s+(?P<Variables>.*?)\>#si", $Contents, $Sections, PREG_SET_ORDER ); //Stage 1, getting the useful data out of the string.

		foreach( $Sections as $Section )
		{
			$Name = $Section["SectionName"];
			$VariablePairs = array();

			preg_match_all( "#(?P<VarName>[a-zA-Z]+\w)+=+\"(?P<VarData>.*?)\"#si", $Section["Variables"], $Settings, PREG_SET_ORDER ); //Stage 2, pick out the individual key/value pairs.

			foreach( $Settings as $Pair )
			{
				$VarName = $Pair["VarName"];
				$VarValue = $Pair["VarData"];

				$VariablePairs[$VarName] = $VarValue;
			}

			$this->ConfigData[$Name] = new ArcemuConfigElement( $VariablePairs );
		}
	}
}

?>